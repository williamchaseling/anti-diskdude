DiskDude! is a ROM management tool from 1996 that placed "DiskDude!" in empty space in the header, which causes issues with some emulators thinking that the header is corrupt.

This Python script was created to make it easy to zero out the "DiskDude!" on many files at once.

Requirements:

Python 3.6+

Installation:

Download "anti-diskdude.py"

Usage:

Drag and drop files onto "anti-diskdude.py"

It should create a file called "anti-diskdude.log" in the directory it is stored in, that can be checked to see what has been touched.


"does not end in .nes" -> not a .nes file, No changes were made

"is clean" -> .nes file, No changes were made

"has been cleaned" -> .nes file, bytes 7-15 were zeroed out.