import sys
def remove_diskdude(file_path):
	# Read the File
	with open(file_path, "rb") as f:
		data = f.read()
	
	# Make sure it says "DiskDude!"
	if data[7:16] == b"DiskDude!":
		# Zero out "DiskDude!"
		with open(file_path, "wb") as f:
			f.write(data[0:7])
			f.write(b"\x00\x00\x00\x00\x00\x00\x00\x00\x00")
			f.write(data[16:])
		log.append(f'Argument "{file_path}" has been cleaned\n')
	else:
		log.append(f'Argument "{file_path}" is clean\n') 

log = []
for arg in sys.argv[1:]:
	# Check file extension is .nes
	if arg[-4:] == ".nes":
		remove_diskdude(arg)
	else:
		log.append(f'Argument "{arg}" does not end in .nes\n')

with open("anti-diskdude.log", "w") as f:
	for line in log:
		f.write(line)
 